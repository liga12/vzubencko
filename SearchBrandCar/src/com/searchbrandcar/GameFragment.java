package com.searchbrandcar;

import com.searchbrandcar.HomeActivity.OnChangeQuestionsListener;
import com.searchbrandcar.Question.ButtonName;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

@SuppressLint("ValidFragment")
public class GameFragment extends Fragment implements OnClickListener{
	
	public static final String TAG = "GameFragment";
	
	private Button btnAnswer1;
	private Button btnAnswer2;
	private Button btnAnswer3;
	private Button btnAnswer4;
	private LinearLayout llAreaButtons;
	
	private ImageView ivCar;
	private Question questions;
	private Animation animOut, animIn,
					  animFadeOut, animFadeIn;
	
	private OnClickAnswerListener answerListener;
	
	public static GameFragment instance;
	
	private GameFragment(){};
	
	public static GameFragment getInstance(){
		if(instance == null){
			instance = new GameFragment();
		}
		return instance;
	}
	
	public static void clearInstance(){
		instance = null;
	}
	
	public void setAnswerClickListener(OnClickAnswerListener listener){
		this.answerListener = listener;
	}
	
	public void setQuestion(Question q){
		this.questions = q;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Log.i(TAG, "onAttach()");
		((HomeActivity)activity).setChangeQuestionsListener(questionsListener);
		
		animOut = AnimationUtils.loadAnimation(activity, android.R.anim.slide_out_right);
		animIn = AnimationUtils.loadAnimation(activity, android.R.anim.slide_in_left);
		animFadeOut = AnimationUtils.loadAnimation(activity, android.R.anim.fade_out);
		animFadeIn = AnimationUtils.loadAnimation(activity, android.R.anim.fade_in);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView()");
		View rootView = inflater.inflate(R.layout.game_fragment, null);
		btnAnswer1 = (Button)rootView.findViewById(R.id.btn_answer1);
		btnAnswer2 = (Button)rootView.findViewById(R.id.btn_answer2);
		btnAnswer3 = (Button)rootView.findViewById(R.id.btn_answer3);
		btnAnswer4 = (Button)rootView.findViewById(R.id.btn_answer4);
		llAreaButtons = (LinearLayout) rootView.findViewById(R.id.ll_area_buttons);
		
		ivCar = (ImageView)rootView.findViewById(R.id.iv_car_image);
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i(TAG, "onActivityCreated()");
		
		btnAnswer1.setOnClickListener(this);
		btnAnswer2.setOnClickListener(this);
		btnAnswer3.setOnClickListener(this);
		btnAnswer4.setOnClickListener(this);
		
		if(questions != null){
			setNamesVariants(questions);
			ivCar.setImageResource(questions.getImage());
		}
	}
	
	private OnChangeQuestionsListener questionsListener = new OnChangeQuestionsListener() {
		
		@Override
		public void OnChange(Question q) {
			setQuestion(q);
			if(questions != null){
				startAnimationChangedOut();
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						ivCar.setImageResource(questions.getImage());
						startAnimationChangedIn();
						setNamesVariants(questions);
					}
				}, 0);
			}
		}
	};
	
	private void startAnimationChangedOut(){
		llAreaButtons.startAnimation(animOut);		
		ivCar.startAnimation(animFadeOut);
	}
	
	private void startAnimationChangedIn(){
		llAreaButtons.startAnimation(animIn);		
		ivCar.startAnimation(animFadeIn);
	}

	private void setNamesVariants(Question q) {
		ButtonName variants = q.getVariantsAnswer();
		btnAnswer1.setText(variants.getVariantAnswer1());
		btnAnswer2.setText(variants.getVariantAnswer2());
		btnAnswer3.setText(variants.getVariantAnswer3());
		btnAnswer4.setText(variants.getVariantAnswer4());
	}

	@Override
	public void onClick(View v) {
		Log.d(TAG, "onClick()");
		switch (v.getId()) {
		case R.id.btn_answer1:
			answerListener.onClick(Question.ANSWER_ONE, questions.getQstId());
			break;
		case R.id.btn_answer2:
			answerListener.onClick(Question.ANSWER_TWO, questions.getQstId());
			break;
		case R.id.btn_answer3:
			answerListener.onClick(Question.ANSWER_THREE, questions.getQstId());
			break;
		case R.id.btn_answer4:
			answerListener.onClick(Question.ANSWER_FOUR, questions.getQstId());
			break;
		}		
	}

	public interface OnClickAnswerListener{
		public void onClick(int answer, int question);
	}
}
