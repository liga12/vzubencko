package com.searchbrandcar;

import android.content.Context;

public class Question {
	
	public static final int ANSWER_ONE = 1;
	public static final int ANSWER_TWO = 2;
	public static final int ANSWER_THREE = 3;
	public static final int ANSWER_FOUR = 4;
	
	public static final int QUESTIONS_1 = 0;
	public static final int QUESTIONS_2 = 1;
	public static final int QUESTIONS_3 = 2;
	public static final int QUESTIONS_4 = 3;
	public static final int QUESTIONS_5 = 4;
	public static final int QUESTIONS_6 = 5;
	public static final int QUESTIONS_7 = 6;
	public static final int QUESTIONS_8 = 7;
	public static final int QUESTIONS_9 = 8;
	public static final int QUESTIONS_10 = 9;
	
	public static final int MAX = 10;
	
	private int image;
	private int correctAnswer;
	private int qstId;
	private ButtonName variantsAnswer;
	private Context mContext;
	
	public Question(Context context, int idQuestions){
		this.mContext = context;
		this.qstId = idQuestions;
		image = getImageForQuestionsById(idQuestions);
		correctAnswer = getCorrectAnswer(idQuestions);
		variantsAnswer = getVariants(idQuestions);
	}

	private int getImageForQuestionsById(int idQuestions){
		int result = -1;
		
		switch (idQuestions) {
		case QUESTIONS_1:return R.drawable.cruiser;
		case QUESTIONS_2:return R.drawable.audi;
		case QUESTIONS_3:return R.drawable.bmw;
		case QUESTIONS_4:return R.drawable.honda;
		case QUESTIONS_5:return R.drawable.kia;
		case QUESTIONS_6:return R.drawable.lotus;
		case QUESTIONS_7:return R.drawable.mazda;
		case QUESTIONS_8:return R.drawable.nissan;
		case QUESTIONS_9:return R.drawable.subaru;
		case QUESTIONS_10:return R.drawable.uaz;
		}
		return result;
	}
	
	private int getCorrectAnswer(int idQuestions) {
		switch (idQuestions) {
		case QUESTIONS_1:return 3;
		case QUESTIONS_2:return 3;
		case QUESTIONS_3:return 1;
		case QUESTIONS_4:return 4;
		case QUESTIONS_5:return 2;
		case QUESTIONS_6:return 3;
		case QUESTIONS_7:return 1;
		case QUESTIONS_8:return 2;
		case QUESTIONS_9:return 1;
		default:return 4;
		}
	}
	
	private ButtonName getVariants(int idQuestions) {
		switch (idQuestions) {
		case QUESTIONS_1:return new ButtonName(mContext.getString(R.string.brand_nissan), 
				   mContext.getString(R.string.brand_mazda), 
				   mContext.getString(R.string.brand_toyota),
				   mContext.getString(R.string.brand_geely));//R.drawable.cruiser; - 3
		case QUESTIONS_2:return new ButtonName(mContext.getString(R.string.brand_kia), 
				   mContext.getString(R.string.brand_ford), 
				   mContext.getString(R.string.brand_audi),
				   mContext.getString(R.string.brand_toyota));//R.drawable.audi; - 3
		case QUESTIONS_3:return new ButtonName(mContext.getString(R.string.brand_bmw), 
				   mContext.getString(R.string.brand_ferrari), 
				   mContext.getString(R.string.brand_toyota),
				   mContext.getString(R.string.brand_geely));//R.drawable.bmw; - 1
		case QUESTIONS_4:return new ButtonName(mContext.getString(R.string.brand_ferrari), 
				   mContext.getString(R.string.brand_mazda),
				   mContext.getString(R.string.brand_audi),
				   mContext.getString(R.string.brand_honda));
		case QUESTIONS_5:return new ButtonName(mContext.getString(R.string.brand_nissan), 
				   mContext.getString(R.string.brand_kia), 
				   mContext.getString(R.string.brand_lotus),
				   mContext.getString(R.string.brand_ferrari));
		case QUESTIONS_6:return new ButtonName(mContext.getString(R.string.brand_geely), 
				   mContext.getString(R.string.brand_subaru), 
				   mContext.getString(R.string.brand_lotus),
				   mContext.getString(R.string.brand_mazda));
		case QUESTIONS_7:return new ButtonName(mContext.getString(R.string.brand_mazda), 
				   mContext.getString(R.string.brand_audi), 
				   mContext.getString(R.string.brand_geely),
				   mContext.getString(R.string.brand_honda));
		case QUESTIONS_8:return new ButtonName(mContext.getString(R.string.brand_kia), 
				   mContext.getString(R.string.brand_nissan), 
				   mContext.getString(R.string.brand_lexus),
				   mContext.getString(R.string.brand_ford));
		case QUESTIONS_9:return new ButtonName(mContext.getString(R.string.brand_subaru), 
				   mContext.getString(R.string.brand_geely), 
				   mContext.getString(R.string.brand_lotus),
				   mContext.getString(R.string.brand_toyota));
		default:return new ButtonName(mContext.getString(R.string.brand_vaz), 
				   mContext.getString(R.string.brand_zaz), 
				   mContext.getString(R.string.brand_laz),
				   mContext.getString(R.string.brand_uaz));
		}
	}

	public int getImage() {
		return image;
	}

	public int getCorrectAnswer() {
		return correctAnswer;
	}
	
	public ButtonName getVariantsAnswer() {
		return variantsAnswer;
	}

	public int getQstId() {
		return qstId;
	}

	public class ButtonName{
		private String variantAnswer1;
		private String variantAnswer2;
		private String variantAnswer3;
		private String variantAnswer4;
		
		public ButtonName(String v1,String v2,String v3,String v4){
			variantAnswer1 = v1;
			variantAnswer2 = v2;
			variantAnswer3 = v3;
			variantAnswer4 = v4;
		}
		public String getVariantAnswer1() {
			return variantAnswer1;
		}
		
		public String getVariantAnswer2() {
			return variantAnswer2;
		}

		public String getVariantAnswer3() {
			return variantAnswer3;
		}

		public String getVariantAnswer4() {
			return variantAnswer4;
		}		
	}
}
