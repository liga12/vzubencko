package com.searchbrandcar;

import java.util.ArrayList;
import java.util.List;

import com.searchbrandcar.GameFragment.OnClickAnswerListener;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

public class HomeActivity extends ActionBarActivity {
	
	private static final String TAG = "HomeActivity";
	
	private int questions = 0;
	private int correctAnswer = 0;
	
	private List<Question> listQuestions;
	private Fragment fragment;
	private OnChangeQuestionsListener changeListener;
	private AlertDialog.Builder builder;
	private AlertDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Log.i(TAG, "onCreate()");
		
		listQuestions = new ArrayList<Question>();
		for(int i = 0; i < Question.MAX; i++){
			listQuestions.add(new Question(HomeActivity.this, i));
		}
		
		initAlertDialogScores();
		
		fragment = GameFragment.getInstance();
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, fragment, GameFragment.TAG).commit();
			((GameFragment)fragment).setAnswerClickListener(answerClickListener);
			((GameFragment)fragment).setQuestion(listQuestions.get(questions));
		}
	}
	
	private void initAlertDialogScores() {
		builder = new AlertDialog.Builder(HomeActivity.this);
		builder.setPositiveButton(R.string.start_again, clickStartAgeinListener);
		builder.setNegativeButton(R.string.exit, clickExitListener);
		builder.setTitle(R.string.congratulation);
		dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
	}
	
	private Spannable getScoresText(int correctAnswer){
		String result = getString(R.string.success_on)+ " " 
					+ (correctAnswer*10) + " %";
		Spannable spanScores = new SpannableString(result);
		spanScores.setSpan(new ForegroundColorSpan(Color.LTGRAY), 0, result.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		spanScores.setSpan(new StyleSpan(Typeface.BOLD), 0, result.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		spanScores.setSpan(new RelativeSizeSpan(2.5f), 0, result.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		if(correctAnswer > 5){
			spanScores.setSpan(new ForegroundColorSpan(Color.GREEN), 12, result.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		} else {
			spanScores.setSpan(new ForegroundColorSpan(Color.RED), 12, result.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		
		return spanScores;
	}

	public void setChangeQuestionsListener(OnChangeQuestionsListener listner){
		this.changeListener = listner;
	}
	
	private OnClickAnswerListener answerClickListener = new OnClickAnswerListener() {
		
		@Override
		public void onClick(int answer, int q) {
			if(q == questions){
				if(answer == listQuestions.get(questions).getCorrectAnswer()){
					correctAnswer++;
				}
				questions++;
				if(questions == Question.MAX){
					dialog.setMessage(getScoresText(correctAnswer));
					dialog.show();
				} else {
					changeListener.OnChange(listQuestions.get(questions));
				}
			}
		}
	};
	
	private OnClickListener clickExitListener = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			finish();
		}
	};
	
	private OnClickListener clickStartAgeinListener = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			correctAnswer = 0;	questions = 0;
			changeListener.OnChange(listQuestions.get(questions));
		}
	};
	
	public interface OnChangeQuestionsListener{
		public void OnChange(Question q);
	}
}
