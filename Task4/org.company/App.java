import java.util.ArrayList;
import java.util.Arrays;

import Departments.Department;
import Employees.Accountant;
import Employees.Employee;
import Employees.Manager;
import Employees.Programmer;

public class App {

    public static void main(String[] args) {

        ArrayList<Employee> employeeArrayList = new ArrayList<Employee>();

        employeeArrayList.add(addManager("Joрn", 10, 135, Department.FINANCE));
        employeeArrayList.add(addManager("Adam", 15, 180, Department.SALES));
        employeeArrayList.add(addManager("Barry", 23, 89, Department.SUPPORT));
        employeeArrayList.add(addManager("Brooke", 60, 210, Department.FINANCE));

        employeeArrayList.add(addProgrammer("Bud", 37, 165, Department.SUPPORT));
        employeeArrayList.add(addProgrammer("Carl", 16, 189, Department.FINANCE));
        employeeArrayList.add(addProgrammer("Calvin", 14, 168, Department.SALES));
        employeeArrayList.add(addProgrammer("Curt", 23, 208, Department.SALES));

        setEmployee(employeeArrayList);
    }

    private static void setEmployee(ArrayList<Employee> employeeArrayList) {
        Accountant accountant = new Accountant();
        String[][] salaryEmployees = accountant.calculateSalaryEmployee(employeeArrayList);
        System.out.println("Salary Employees = " + Arrays.deepToString(salaryEmployees));
    }

    private static Employee addManager(String name, double plan, double timeWork, Department department) {
        Employee manager = new Manager(plan, timeWork, department, name);
        System.out.println(manager.toString());
        return manager;
    }

    private static Employee addProgrammer(String name, double plan, double timeWork, Department department) {
        Employee programmer = new Programmer(plan, timeWork, department, name);
        System.out.println(programmer.toString());
        return programmer;
    }
}
