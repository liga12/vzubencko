package Employees;

import Departments.Department;

public abstract class Employee {

    public double salary;
    private double rate;
    private double timeWork;
    private double percentOfTimeWork;
    private double plan;
    private double planTimeWork = 160;
    private Department department;
    private String name;

    Employee(double plan, double timeWork, Department department, String name) {
        this.plan = plan;
        this.timeWork = timeWork;
        this.department = department;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public double getPlanTimeWork() {
        return planTimeWork;
    }

    public double getPlan() {
        return plan;
    }

    public double getRate() {
        rate = planTimeWork * plan;
        return rate;
    }

    public double getTimeWork() {
        return timeWork;
    }

    public double getPercentOfTimeWork() {
        percentOfTimeWork = timeWork / planTimeWork;
        return percentOfTimeWork;
    }

    public abstract Double getSalary();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (Double.compare(employee.timeWork, timeWork) != 0) return false;
        return Double.compare(employee.plan, plan) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(timeWork);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(plan);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}