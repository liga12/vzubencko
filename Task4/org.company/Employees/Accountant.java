package Employees;

import java.util.ArrayList;

import Departments.Department;

public class Accountant {
    double salaryFinace;
    double salarySales;
    double salarySuppord;

    public String[][] calculateSalaryEmployee(ArrayList<Employee> employeeArrayList) {

        int depLenght = Department.values().length;
        String[][] salaryByDepartment = new String[depLenght][2];
        for (Employee emp : employeeArrayList) {
            switch (emp.getDepartment()) {
                case FINANCE: {
                    salaryFinace += emp.getSalary();
                    break;
                }
                case SUPPORT: {
                    salarySuppord += emp.getSalary();
                    break;
                }
                case SALES: {
                    salarySales += emp.getSalary();
                    break;
                }
                default:
                    System.out.println("Error Department ");
            }

            salaryByDepartment[0][0] = String.valueOf(Department.FINANCE);
            salaryByDepartment[0][1] = String.valueOf(salaryFinace);
            salaryByDepartment[1][0] = String.valueOf(Department.SUPPORT);
            salaryByDepartment[1][1] = String.valueOf(salarySuppord);
            salaryByDepartment[2][0] = String.valueOf(Department.SALES);
            salaryByDepartment[2][1] = Double.toString(salarySales);
        }
        return salaryByDepartment;
    }
}
