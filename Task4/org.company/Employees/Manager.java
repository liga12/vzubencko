package Employees;

import Departments.Department;

public class Manager extends Employee {
    public Manager(double plan, double timeWork, Department department, String name) {
        super(plan, timeWork, department, name);
    }

    @Override
    public Double getSalary() {
        if (getTimeWork() >= getPlanTimeWork()) {
            salary = getRate();
        } else if (getTimeWork() < getPlanTimeWork()) {
            salary = getPercentOfTimeWork() * getRate();
        }
        return salary;
    }

    @Override
    public String toString() {
        return "Manager [" + getName() + " " + getDepartment() + "]";
    }
}
