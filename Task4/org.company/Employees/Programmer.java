package Employees;

import Departments.Department;

public class Programmer extends Employee {


    public Programmer(double plan, double timeWork, Department department, String name) {
        super(plan, timeWork, department, name);
    }

    @Override
    public Double getSalary() {
        salary = getPercentOfTimeWork() * getRate();
        return salary;
    }

    @Override
    public String toString() {
        return "Programmer[" + getName() + " " + getDepartment() + "]";
    }
}
