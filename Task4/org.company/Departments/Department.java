package Departments;

public enum Department {
    FINANCE("2356", "54456456546"), SALES("5567", "546546546"), SUPPORT("5677", "56456546546");
    private String officeNumber;
    private String phoneNumber;

    Department(String officeNumber, String phoneNumber) {
        this.setOfficeNumber(officeNumber);
        this.setPhoneNumber(phoneNumber);
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}