package Course;

import static java.lang.System.out;

public class Task2Variant1 {
    public static void main(String[] args) {
        new Task2Variant1().printPyramid( (int) (Math.random() * 31));
    }

    private void printPyramid(int a) {
        int count = -1;
        if (a % 2 > 0) {
            for (int i = 0; i <= a; i++) {
                count++;
                if (i > a / 2){
                    count -= 2;
                }
                for (int j = 0; j <= count; j++) {
                    out.print("*");
                }
                out.print("\n");
            }
        } else  out.println("an even number of rows");
    }
}

