package Course;

public class Supplementary {
    private boolean result;
    public static void main(String[] args) {

        boolean[] array = new boolean[]{false, false,true,false};

        for (int  i =0; i < array.length; i++ ){
            boolean a = true;
            boolean b = false;

            if (i <= 2 ){
            a = array[i];
            b= array[i+1];
            }
            if (i > 2 ){
            a = array[i-1];
            b = a;
            }

            System.out.println( a + "&&" + b +" = " + new Supplementary().and(a, b));
            System.out.println( a + "||" + b +" = " + new Supplementary().or(a, b));
        }
    }

    private  boolean and(boolean x, boolean y) {
        if (!x) {
            return false;
        } else {
            return y;
        }
    }

    private boolean or(boolean x, boolean y){
        if(x){
            return true;
        } else {
            return y;
        }
    }
}
