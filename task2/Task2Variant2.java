package Course;

public class Task2Variant2 {
    public static void main(String[] args) {

        int[] number = new int[]{12, -455, 697, 89, -4561, -1654};
        new Task2Variant2().palindromeOnCheck(number);
    }

   private static void palindromeOnCheck(int[] number) {
        for (int i =0; i < number.length-1; i++){
            if (number[i] == inverse( number[i+1] ))
                System.out.println(number[i] + " and " + number[i+1] + " polindrome");
            else System.out.println(number[i] + " and " + number[i+1] + " don't polindrome");
        }
   }

    private static int inverse(int value) {
        int result = 0;
        while (value != 0) {
            result = result * 10 + value % 10;
            value /= 10;
        }
        return result;
    }
}