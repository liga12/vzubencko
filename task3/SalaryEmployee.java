package Course.task3;

public interface SalaryEmployee {

    void salaryManager(int[][] s);

    void salaryProgrammer(int[][] p);

}
