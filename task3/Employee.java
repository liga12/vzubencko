package Course.task3;

/**
 * Created by LIGA on 23.08.2016.
 */
public abstract class Employee {
    private int rate;
    private double percentageTime;
    public int maxTime = 160;
    private int ratePerHour;
    private int timeWork;
    protected double salary;

Employee(int ratePerHour,int timeWork ){
    this.ratePerHour = ratePerHour;
    this.timeWork = timeWork;
}
    public int getRatePerHour(){
        return ratePerHour;
    }

    public int getRate() {
        return rate =  maxTime * ratePerHour;
    }

    public int getTimeWork() {
        return timeWork;
    }

    public void calculatePercentageTime(){
        percentageTime = (100 * (double)timeWork)/(double) maxTime;
    }

    public double getPercentageTime() {
        return  percentageTime;
    }

    public abstract double  getSalary();
}
