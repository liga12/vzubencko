package Course.task3;

public class Accountant implements SalaryEmployee {

    @Override
    public void salaryManager(int[][] s) {

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 1; j++) {
                Employee manager = new Manager( s[i][j], s[i][j + 1] );
                System.out.println( "Maneger" + "[" + (i + 1) + "]" + "      salary = " + manager.getSalary() );
            }
        }
    }

    @Override
    public void salaryProgrammer(int[][] p) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 1; j++) {
                Employee programmer = new Programmer( p[i][j], p[i][j + 1] );
                System.out.println( "Programmer" + "["+ (i+1) + "]" + "   salary = " + programmer.getSalary() );
            }
        }
    }
}
