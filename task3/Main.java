package Course.task3;

/**
 * Created by LIGA on 24.08.2016.
 */
public class Main {
    static int[][] scheduleEmployee = new int[][]{{10,135},{35,110},{56,189},{8,114},{17,197}};
    public static void main(String[] args) {

        SalaryEmployee accountant = new Accountant();
        accountant.salaryManager(scheduleEmployee);
        accountant.salaryProgrammer(scheduleEmployee);

    }
}
