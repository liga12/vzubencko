package Course.task3;

public class Programmer extends Employee {

    Programmer( int ratePerHour, int timeWork ){
        super(ratePerHour,timeWork);
    }

    @Override
    public double getSalary() {
        salary = getTimeWork()*getRatePerHour();
        return salary ;
    }
}
