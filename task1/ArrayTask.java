package Course;


import java.util.Arrays;

/**
 * Created by LIGA on 17.08.2016.
 */
public class ArrayTask {

    int[] array;
    int[] reformedArray = new int[2];

    public static void main(String[] args) {
        int quantityElement = 20;

        ArrayTask arrayTask = new ArrayTask();
        arrayTask.receiptArray(quantityElement);
        System.out.println("Source array = " + Arrays.toString(arrayTask.array));
        arrayTask.transformationArray(arrayTask.array);
        System.out.println("The sum of all the even elements of the array and the number of = "
                + Arrays.toString(arrayTask.reformedArray));
    }

    public int[] receiptArray(int kolNum) {
        array = new int[kolNum];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) ((Math.random()) * 100);
        }
        return array;
    }

    public int[] transformationArray(int[] array) {
        int count = 0;
        int sum = 0;

        for (int i = 0; i < array.length; i = i + 2) {
            sum = sum + array[i];
            count++;
        }
        reformedArray[1] = count;
        reformedArray[0] = sum;
        return reformedArray;
    }
}