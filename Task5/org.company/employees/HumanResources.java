package employees;

import java.time.LocalDate;
import java.time.Month;

public class HumanResources {
    LocalDate birthDay;
    LocalDate toDayBirthday = LocalDate.of(2012, Month.APRIL, 26);
    LocalDate toDayWorkAnniversary = LocalDate.of(2012, Month.NOVEMBER, 26);
    LocalDate startWork;

    public String celebrationBirthDay(LocalDate date) {
        if ((date.getMonth() == toDayBirthday.getMonth()) && (date.getDayOfMonth() == toDayBirthday.getDayOfMonth())) {
            return "Yes";
        } else {
            return "";
        }
    }

    public String celebrationWorkAnniversary(LocalDate date) {
        if (toDayWorkAnniversary.minusYears(1).isEqual(date) ||
                toDayWorkAnniversary.minusYears(3).isEqual(date) ||
                toDayWorkAnniversary.minusYears(5).isEqual(date) ||
                toDayWorkAnniversary.minusYears(10).isEqual(date) ||
                toDayWorkAnniversary.minusYears(25).isEqual(date)) {
            return "Yes";
        } else {
            return "";
        }
    }

    public LocalDate setBirthday(int year, Month month, int date) {
        birthDay = LocalDate.of(year, month, date);
        return birthDay;
    }

    public LocalDate setStartWork(int year, Month month, int date) {
        startWork = LocalDate.of(year, month, date);
        return startWork;
    }
}
