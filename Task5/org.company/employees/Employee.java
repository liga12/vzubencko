package employees;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import departments.DepartmentType;

public abstract class Employee {

    private final Double planTimeWork = new Double(160);

    protected List<Programmer> programmerList = new ArrayList<>();
    protected ArrayList<Manager> managerList = new ArrayList<>();

    protected Double salary;
    private Double rate;
    private Double timeWork;
    private Double percentOfTimeWork;
    private Double plan;
    private DepartmentType department;
    private String name;
    private LocalDate birthDay;
    private LocalDate startWork;

    protected Employee(double plan, double timeWork, DepartmentType department, String name, LocalDate birthDay, LocalDate startWork) {
        this.plan = plan;
        this.timeWork = timeWork;
        this.department = department;
        this.name = name;
        this.birthDay = birthDay;
        this.startWork = startWork;

    }

    protected Employee() {
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public LocalDate getStartWork() {
        return startWork;
    }

    public String getName() {
        return name;
    }

    public DepartmentType getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentType department) {
        this.department = department;
    }

    public double getPlanTimeWork() {
        return planTimeWork;
    }

    public double getPlan() {
        return plan;
    }

    public double getRate() {
        rate = planTimeWork * plan;
        return rate;
    }

    public double getTimeWork() {
        return timeWork;
    }

    public double getPercentOfTimeWork() {
        percentOfTimeWork = timeWork / planTimeWork;
        return percentOfTimeWork;
    }

    public abstract Double getSalary();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (Double.compare(employee.timeWork, timeWork) != 0) return false;
        return Double.compare(employee.plan, plan) == 0;
    }


    public List<Manager> getManagerList() {
        return managerList;
    }

    public void setManagerList(ArrayList<Manager> managerList) {

        this.managerList = managerList;
    }

    public List<Programmer> getProgrammerList() {
        return programmerList;
    }

    public void setProgrammerList(List<Programmer> programmerList) {
        this.programmerList = programmerList;
    }
}
