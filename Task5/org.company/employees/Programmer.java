package employees;

import java.time.LocalDate;

import departments.DepartmentType;

public class Programmer extends Employee {

    public Programmer(String name, double plan, double timeWork, DepartmentType department, LocalDate birthDay, LocalDate startWork) {
        super(plan, timeWork, department, name, birthDay, startWork);
    }

    public Programmer() {
        super();
    }

    @Override
    public Double getSalary() {
        salary = getPercentOfTimeWork() * getRate();
        return salary;
    }

    @Override
    public String toString() {
        return "Programmer";
    }
}
