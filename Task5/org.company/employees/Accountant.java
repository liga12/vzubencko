package employees;

import java.util.Arrays;
import java.util.List;

import departments.DepartmentType;

public class Accountant {

    public String[][] calculateSalaryEmployee(List<List<? extends Employee>> employeeArrayList) {
        double salaryFinance = 0, salarySupport = 0, salarySales = 0;
        int depLength = DepartmentType.values().length;
        String[][] salaryByDepartment = new String[depLength][2];

        for (List<? extends Employee> emp : employeeArrayList) {
            for (Employee employee : emp) {
                switch (employee.getDepartment()) {
                    case FINANCE: {
                        salaryFinance += employee.getSalary();
                        break;
                    }
                    case SUPPORT: {
                        salarySupport += employee.getSalary();
                        break;
                    }
                    case SALES: {
                        salarySales += employee.getSalary();
                        break;
                    }
                    default:
                        System.out.println("Error Department ");
                }
            }
        }

        salaryByDepartment[0][0] = String.valueOf(DepartmentType.FINANCE);
        salaryByDepartment[0][1] = String.valueOf(salaryFinance);
        salaryByDepartment[1][0] = String.valueOf(DepartmentType.SUPPORT);
        salaryByDepartment[1][1] = String.valueOf(salarySupport);
        salaryByDepartment[2][0] = String.valueOf(DepartmentType.SALES);
        salaryByDepartment[2][1] = String.valueOf(salarySales);

        System.out.println("\nSalary employees by department = " +
                Arrays.deepToString(salaryByDepartment));
        return salaryByDepartment;
    }
}
