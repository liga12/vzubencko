package employees;

import java.time.LocalDate;

import departments.DepartmentType;

public class Manager extends Employee {
    public Manager(String name, double plan, double timeWork, DepartmentType department, LocalDate birthday, LocalDate startWork) {
        super(plan, timeWork, department, name, birthday, startWork);
    }

    public Manager() {
    }

    @Override
    public Double getSalary() {
        if (getTimeWork() >= getPlanTimeWork()) {
            salary = getRate();
        } else if (getTimeWork() < getPlanTimeWork()) {
            salary = getPercentOfTimeWork() * getRate();
        }
        return salary;
    }

    @Override
    public String toString() {

        return "Manager";
    }
}
