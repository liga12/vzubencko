import java.time.Month;

import departments.Department;
import employees.Accountant;
import departments.DepartmentType;
import employees.Employee;
import employees.HumanResources;
import employees.Manager;
import employees.Programmer;
import format.Format;

public class App {

    public static void main(String[] args) {

        HumanResources human = new HumanResources();
        Employee employeeManager = new Manager();
        Employee employeeProgrammer = new Programmer();
        Department department = new Department();

        employeeManager.getManagerList().add(new Manager("John", 10, 135, DepartmentType.FINANCE,
                human.setBirthday(1991, Month.APRIL, 25), human.setStartWork(2011, Month.APRIL, 26)));
        employeeManager.getManagerList().add(new Manager("Adam", 15, 180, DepartmentType.SALES,
                human.setBirthday(1992, Month.APRIL, 25), human.setStartWork(2010, Month.NOVEMBER, 26)));
        employeeManager.getManagerList().add(new Manager("Barry", 23, 89, DepartmentType.SUPPORT,
                human.setBirthday(1993, Month.APRIL, 27), human.setStartWork(2009, Month.NOVEMBER, 26)));
        employeeManager.getManagerList().add(new Manager("Brooke", 60, 210, DepartmentType.FINANCE,
                human.setBirthday(2011, Month.APRIL, 26), human.setStartWork(2008, Month.NOVEMBER, 26)));

        employeeProgrammer.getProgrammerList().add(new Programmer("Bud", 37, 165, DepartmentType.SUPPORT,
                human.setBirthday(1995, Month.APRIL, 25), human.setStartWork(2007, Month.NOVEMBER, 26)));
        employeeProgrammer.getProgrammerList().add(new Programmer("Carl", 16, 189, DepartmentType.FINANCE,
                human.setBirthday(1996, Month.APRIL, 25), human.setStartWork(2006, Month.NOVEMBER, 26)));
        employeeProgrammer.getProgrammerList().add(new Programmer("Calvin", 14, 129, DepartmentType.SALES,
                human.setBirthday(1996, Month.APRIL, 25),human.setStartWork(2005, Month.NOVEMBER, 26)));
        employeeProgrammer.getProgrammerList().add(new Programmer("Curt", 23, 208, DepartmentType.SALES,
                human.setBirthday(1997, Month.APRIL, 25), human.setStartWork(2004, Month.NOVEMBER, 26)));

        department.getEmployeeList().add(employeeManager.getManagerList());
        department.getEmployeeList().add(employeeProgrammer.getProgrammerList());

        new Format().createTable(department.getEmployeeList());
        new Accountant().calculateSalaryEmployee(department.getEmployeeList());
    }
}
