package departments;

import java.util.ArrayList;
import java.util.List;

import employees.Employee;
import employees.Manager;

public class Department extends Employee {

    private String officeNumber;
    private String phoneNumber;

    private List<List<? extends Employee>> employeeList = new ArrayList<>();


    public Department(String officeNumber, String phoneNumber) {
        this.setOfficeNumber(officeNumber);
        this.setPhoneNumber(phoneNumber);
    }

    public Department(List<Manager> managerList) {
    }

    public Department() {
    }

    @Override
    public Double getSalary() {
        return null;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<List<? extends Employee>> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<List<? extends Employee>> employeeList) {
        this.employeeList = employeeList;
    }
}