package format;

import java.util.List;

import employees.Employee;
import employees.HumanResources;

public class Format {

    public int numberEmployee;
    HumanResources humanResources = new HumanResources();

    public void createTable(List<List<? extends Employee>> employeeArrayList) {
        System.out.printf("|%s|%-6s|%-10s|%-7s|%-16s|%-14s|%-16s|%n", "#", "Name", "Profession",
                "Rate", "Salary per month", "Birthday", "Work anniversary");
        System.out.println("------------------------------------------------------------------------------");
        int counter = 0;
        for (List<? extends Employee> emp : employeeArrayList) {
            for (Employee employee : emp) {
                counter++;
                System.out.printf("|%d|%-6s|%-10s|%-7.2f|%-16.2f|%-14s|%-16s|", (counter), employee.getName(),
                        employee.toString(), employee.getRate(), employee.getSalary(),
                        humanResources.celebrationBirthDay(employee.getBirthDay()),
                        humanResources.celebrationWorkAnniversary(employee.getStartWork()));
                if (counter < (employeeArrayList.size()+1 + emp.size()+1)){
                    System.out.println();
                }
            }
        }
    }
}
